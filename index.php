<?php get_header(); ?>

<main>
    <div class="top">
        <div class="q-1"><p>we'll be live soon</p></div>

        <div class="q-2">
            <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/tivoli-logo.svg';?>" alt="Tivoli - Expand Your World">
        </div>
    </div>

    <div class="bottom">
        <div class="q-3">
            <p>worry-free<br>money exchange<br>experience</p>
        </div>

        <div class="q-4">
            <p>+63917.813.4723</p>
            <p><a href="mailto:hello@tivoli.ph">hi@tivoli.ph</p>
        </div>
    </div>
</main>

<?php get_footer();
